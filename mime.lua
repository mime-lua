#!/usr/bin/lua
--
-- Copyright (c) 2009 Mauro Iazzi
--
-- Permission is hereby granted, free of charge, to any person
-- obtaining a copy of this software and associated documentation
-- files (the "Software"), to deal in the Software without
-- restriction, including without limitation the rights to use,
-- copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
-- OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
-- HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
-- WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
-- OTHER DEALINGS IN THE SOFTWARE.
--/

require 'lpeg'

-- MIME implementation in Lua

-- XXX: at section 3.4


local string = string
local type = type
local tostring = tostring
local select = select
local setfenv = setfenv
local CRLF = '\r\n'
local lpeg = lpeg
local print = print

-- this creates a named and substitution capture for the given pattern
lpeg.Csg = function (patt, name)
	return (#lpeg.Cg(patt, name)) * lpeg.Cs(patt)
end
-- this collects both the table of named captures and the
-- substitution capture of the given pattern
lpeg.Cst = function (patt)
	return (#lpeg.Cs(patt) * lpeg.Ct(patt)) / function (s, t)
		local ret = {}
		ret [1] = s
		for k, v in pairs(t) do
			if type(k)~='number' then
				ret[k] = v
			end
		end
		return ret
	end
end

-- rfc2822 Section 2.1:
--
-- A message consists of header fields (collectively called "the header
-- of the message") followed, optionally, by a body.  The header is a
-- sequence of lines of characters with special syntax as defined in
-- this standard. The body is simply a sequence of characters that
-- follows the header and is separated from the header by an empty line
-- (i.e., a line with nothing preceding the CRLF).
--
local split_content = function (c)
	-- NOTE: there is no guarantee that the CRLF separating header
	-- and body is the first one. However no headers admit a blank
	-- line so far
	local h, b = string.match(c, '^(.-)'..CRLF..CRLF..'(.*)$')
	assert(type(h)=='string' and type(b)=='string')
	return h, b
end

-- rfc2822 Section 2.2.3
--
-- The process of moving from this folded multiple-line representation
-- of a header field to its single line representation is called
-- "unfolding". Unfolding is accomplished by simply removing any CRLF
-- that is immediately followed by WSP.  Each header field should be
-- treated in its unfolded form for further syntactic and semantic
-- evaluation.
--
local unfold_header = function (h)
	return string.gsub(h, CRLF..'[ \t]', ' ')
end

-- rfc2822 Section 2.1
--
-- At the most basic level, a message is a series of characters.  A
-- message that is conformant with this standard is comprised of
-- characters with values in the range 1 through 127 and interpreted as
-- US-ASCII characters [ASCII].  For brevity, this document sometimes
-- refers to this range of characters as simply "US-ASCII characters".
--

-- rfc2822 Section 2.1.1
--
-- There are two limits that this standard places on the number of
-- characters in a line. Each line of characters MUST be no more than
-- 998 characters, and SHOULD be no more than 78 characters, excluding
-- the CRLF.
--
local check_content = function (c)
	local err, warn
	-- TODO: implement in a portable way
	return err, warn
end

-- rfc2822 Section 2.3
--
-- The body of a message is simply lines of US-ASCII characters.  The
-- only two limitations on the body are as follows:
--
-- - CR and LF MUST only occur together as CRLF; they MUST NOT appear
-- independently in the body.
--
-- - Lines of characters in the body MUST be limited to 998 characters,
--
-- and SHOULD be limited to 78 characters, excluding the CRLF.
--
local check_body = function (b)
	local err, warn
	-- TODO: implement
	return err, warn
end

-- some core values (not implemented literally)
-- see RFC 2234 Section 6.1
local core_values = {
	["ALPHA"] = lpeg.R("AZ", "az"),
	["BIT"] = lpeg.P("0") + lpeg.P("1"),
	["CHAR"] = lpeg.R("\01\127"),
	["CR"] = lpeg.P("\13"),
	["CRLF"] = lpeg.P("\13\10"),
	["DIGIT"] = lpeg.R("09"),
	["LF"] = lpeg.P("\10"),
	["WSP"] = lpeg.S("\32\09"),
}

-- lexical tokens used in the specification
-- TODO: could be optimized
-- check for non-obfuscated optimizations
-- TODO: write in a less obfuscated way
-- check if parenthesis can be removed
local lex_tokens = function ()
	setfenv(1, lpeg)
	return {
	-- control characters without whitespaces
	["NO-WS-CTL"] =	R("\1\8") +
	P("\11") +
	P("\12") +
	R("\14\13") +
	P("\127"), -- RFC 2822 Section 3.2.1
	-- a character in a text
	["text"] =	R("\1\9") +
	P("\11") +
	P("\12") +
	R("\14\127"), -- RFC 2822 Section 3.2.1
	-- a special character
	["specials"] =	P("(") + P(")") +
	P("<") + P(">") +
	P("[") + P("]") +
	P(":") + P(";") +
	P("@") + P("\\") +
	P(",") + P(".") +
	P("\""), -- RFC 2822 Section 3.2.1
	-- a quoted pair should return only the second character
	["quoted-pair"] = (P("\\")/'' * V("text")) + V("obs-qp"), -- RFC 2822 Section 3.2.2
	-- a folding white space (a whitespace that can include a CRLF)
	-- should be substituted by a single whitespace
	["FWS"] = (((V("WSP")^0 * V("CRLF"))^-1 * V("WSP")^1) + V("obs-FWS")) / " ", -- RFC 2822 Section 3.2.3
	-- a text character allowed inside a comment
	["ctext"] =	V("NO-WS-CTL") +
	R("\33\39") +
	R("\42\91") +
	R("\93\126"), -- RFC 2822 Section 3.2.3
	-- the content of a comment (comments can nest)
	["ccontent"] =	V("ctext") + V("quoted-pair") + V("comment"), -- RFC 2822 Section 3.2.3
	-- an actual comment
	-- should be substituted by a single whitespace
	["comment"] =	(P("(") * (V("FWS")^-1 * V("ccontent"))^0 * V("FWS")^-1 * P(")")) / " ", -- RFC 2822 Section 3.2.3
	-- a comment or a folding white space
	-- should be substituted by a single whitespace
	--
	-- Folding white spaces should not be placed in a way that
	-- creates lines containing only whitespaces.
	-- This requirement Is not necessarily enforced by this grammar
	["CFWS"] =	( (V("FWS") + (V("comment")*V("FWS")^-1)) * (V("comment")*V("FWS")^-1)^0 ) / " ", -- RFC 2822 Section 3.2.3
	-- character that can appear in an atom
	["atext"] =	V("ALPHA") + V("DIGIT") +
	P("!") + P("#") +
	P("$") + P("%") +
	P("&") + P("'") +
	P("*") + P("+") +
	P("-") + P("/") +
	P("=") + P("?") +
	P("^") + P("_") +
	P("`") + P("{") +
	P("|") + P("}") +
	P("~"), -- RFC 2822 Section 3.2.4
	-- an atom is equal to the content only discarding comments and whitespace
	["atom"] =	V("CFWS")^-1 * C(V("atext")^1) * V("CFWS")^-1, -- RFC 2822 Section 3.2.4
	-- an atom with dots is only the content, discarding CFWSs
	["dot-atom"] =	V("CFWS")^-1 * C(V("dot-atom-text")) * V("CFWS")^-1, -- RFC 2822 Section 3.2.4
	-- the content of an atom text with dots
	["dot-atom-text"] =	V("atext")^1 * (P(".") * V("atext")^1)^0, -- RFC 2822 Section 3.2.4
	-- character that can appear in a quoted string
	["qtext"] =	V("NO-WS-CTL") +
	P("\33") +
	R("\35\91") +
	R("\93\126"), -- RFC 2822 Section 3.2.5
	-- character or quoted pair (both can appear in a quoted string)
	-- it is equivalent to the character itself or to the result of the
	-- quoted pair
	["qcontent"] =	C(V("qtext")) + V("quoted-pair"),  -- RFC 2822 Section 3.2.5
	-- a quoted string is equal to its content
	["quoted-string"] =	V("CFWS")^-1 *
	P("\"") * (V("FWS")^-1 * V("qcontent"))^0 * V("FWS")^-1 * P("\"") * V("CFWS")^-1, -- RFC 2822 Section 3.2.5
	-- unstructured patterns for unspecified headers
	-- what should these be equal to?
	--
	-- an generic word
	["word"] =	V("atom") + V("quoted-string"), -- RFC 2822 Section 3.2.6
	-- an generic phrase
	["phrase"] =	Cs(V("word")^1) + V("obs-phrase"), -- RFC 2822 Section 3.2.6
	-- a character for unstructured text
	["utext"] =	V("NO-WS-CTL") + R("\33\126") + V("obs-utext"), -- RFC 2822 Section 3.2.6
	-- an unstructured text
	["unstructured"] =	(V("FWS")^-1 * V("utext"))^0 * V("FWS")^-1, -- RFC 2822 Section 3.2.6
}
end

local deb = function(...) for i=1,select('#',...) do local v=select(i,...)print(type(v), v)end end

local date_time = function ()
	setfenv(1, lpeg)
	return {
	-- date and time specification
	-- dates and times should be valid
	-- this grammar does not enforce this yet
	["date-time"] = Cst(( V"day-of-week" * P"," )^-1 * V"date" * V"FWS" * V"time" * V"CFWS"^-1), -- RFC 2822 Section 3.3
	["day-of-week"] = ( V"FWS"^-1 * V"day-name" ) + V"obs-day-of-week", -- RFC 2822 Section 3.3
	["day-name"] = Cg(P"Mon" + P"Tue" + P"Wed" + P"Thu" + P"Fri" + P"Sat" + P"Sun", "weekday"), -- RFC 2822 Section 3.3
	["date"] = V"day" * V"month" * V"year", -- RFC 2822 Section 3.3
	["year"] = Cg(C(V"DIGIT"^4), "year") + V"obs-year", -- RFC 2822 Section 3.3
	["month"] = (V"FWS" * V"month-name" * V"FWS") + V"obs-month", -- RFC 2822 Section 3.3
	["month-name"] = Cg(C"Jan" + C"Feb" + C"Mar" + C"Apr" + C"May" + C"Jun" + C"Jul" + C"Aug" + C"Sep" + C"Oct" + C"Nov" + C"Dec", "month"), -- RFC 2822 Section 3.3
	["day"] = (V"FWS"^-1 * Cg(C(V"DIGIT" * V"DIGIT"^-1), "day")) + V"obs-day", -- RFC 2822 Section 3.3
	["time"] = V"time-of-day" * V"FWS" * V"zone", -- RFC 2822 Section 3.3
	["time-of-day"] = V"hour" * P":" * V"minute" * (P":" * V"second")^-1, -- RFC 2822 Section 3.3
	["hour"] = Cg(C(V"DIGIT" * V"DIGIT"), "hour") + V"obs-hour", -- RFC 2822 Section 3.3
	["minute"] = Cg((V"DIGIT" * V"DIGIT"), "minute") + V"obs-minute", -- RFC 2822 Section 3.3
	["second"] = Cg((V"DIGIT" * V"DIGIT"), "second") + V"obs-second", -- RFC 2822 Section 3.3
	["zone"] = Cg( (P"+" + P"-") * V"DIGIT" * V"DIGIT" * V"DIGIT" * V"DIGIT", "zone" ) + V"obs-zone", -- RFC 2822 Section 3.3
}
end

local address = function ()
	setfenv(1, lpeg)
	return {
	-- address specification
	-- dates and times should be valid
	-- this grammar does not enforce this yet
	["address"] = V"mailbox" + V"group", -- RFC 2822 Section 3.4
	["mailbox"] = V"name-addr" + V"addr-spec", -- RFC 2822 Section 3.4
	["name-addr"] = V"display-name"^-1 * V"angle-addr", -- RFC 2822 Section 3.4
	["angle-addr"] = (V"CFWS"^-1 * P"<" * V"addr-spec" * P">" * V"CFWS"^-1) + V"obs-angle-addr", -- RFC 2822 Section 3.4
	["group"] = V"display-name" * P":" * (V"mailbox-list" + V"CFWS") * P";" * V"CFWS"^-1, -- RFC 2822 Section 3.4
	["display-name"] = V"phrase", -- RFC 2822 Section 3.4
	["mailbox-list"] = (V"mailbox" * (P"," * V"mailbox")^0) + V"obs-mailbox-list", -- RFC 2822 Section 3.4
	["address-list"] = (V"address" * (P"," * V"address")^0) + V"obs-address-list", -- RFC 2822 Section 3.4
	-- address specification (name@host.domain)
	["addr-spec"] = Cst(V"local-part" * P"@" * V"domain"), -- RFC 2822 Section 3.4.1
	["local-part"] = Csg(V"dot-atom" + V"quoted-string" + V"obs-local-part", "box"), -- RFC 2822 Section 3.4.1
	["domain"] = Cg(V"dot-atom" + V"domain-literal" + V"obs-domain", "domain"), -- RFC 2822 Section 3.4.1
	["domain-literal"] = Cs(V"CFWS"^-1 * P"[" * (V"FWS"^-1 * V"dcontent")^0 * V"FWS"^-1 * P"]" * V"CFWS"^-1), -- RFC 2822 Section 3.4.1
	["dcontent"] = V"dtext" + V"quoted-pair", -- RFC 2822 Section 3.4.1
	["dtext"] = V"NO-WS-CTL" + R"\33\90" + R"\94\126", -- RFC 2822 Section 3.4.1
}
end

local overall_message = function ()
	setfenv(1, lpeg)
	return {
	-- overall message specification
	--
	-- RRC 2822 Section 3.5:
	--
	-- A message consists of header fields, optionally followed by a message
	-- body.  Lines in a message MUST be a maximum of 998 characters
	-- excluding the CRLF, but it is RECOMMENDED that lines be limited to 78
	-- characters excluding the CRLF.
	--
	-- this grammar does not enforce the 78 chars limit yet
	["message"] = (V"fields" + V"obs-fields") * (V"CRLF" * V"body")^-1, -- RFC 2822 Section 3.5
	["body"] = (V"text"^-998 * V"CRLF")^0 * V"text"^-998, -- RFC 2822 Section 3.5
}
end

local fields = function ()
	setfenv(1, lpeg)
	return {
	-- spec for all fields in the header
	--
	-- RRC 2822 Section 3.6
	--
	--the "fields" rule is incomplete
	["fields"] = -- trace fields missing
			(V"orig-date" + V"from" + V"sender" + V"reply-to" + V"to" + V"cc" + V"bcc"
			+ V"message-id" + V"in-reply-to" + V"to" + V"cc" + V"bcc")^0, -- RFC 2822 Section 3.6
	-- origination date field: the time at which the author decided
	-- the message was ready to be sent (e.g. hit the button)
	["orig-date"] = P"Date:" * V"date-time" * V"CRLF", -- RFC 2822 Section 3.6.1
	-- the author of the message
	["from"] = P"From:" * V"mailbox-list" * V"CRLF", -- RFC 2822 Section 3.6.2
	-- the actual agent that sent the message
	["sender"] = P"Sender:" * V"mailbox" * V"CRLF", -- RFC 2822 Section 3.6.2
	-- address which should be replied to
	-- see Section 3.6.3 for forming replies
	["reply-to"] = P"Reply-To:" * V"address-list" * V"CRLF", -- RFC 2822 Section 3.6.2
	-- The "To:" field contains the address(es) of the primary recipient(s)
	-- of the message.
	["to"] = P"To:" * V"address-list" * V"CRLF", -- RFC 2822 Section 3.6.3
	-- The "Cc:" field contains the addresses of others who are to receive
	-- the message, though the content of the message may not be directed
	-- at them.
	["cc"] = P"Cc:" * V"address-list" * V"CRLF", -- RFC 2822 Section 3.6.3
	-- The "Bcc:" field contains addresses of recipients of the message
	-- whose addresses are not to be revealed to other recipients of the
	-- message.
	["bcc"] = P"Bcc:" * (V"address-list" + V"CFWS") * V"CRLF", -- RFC 2822 Section 3.6.3
	["message-id"] = P"Message-Id:" * V"msg-id" * V"CRLF", -- RFC 2822 Section 3.6.4
	["in-reply-to"] = P"In-Reply-To:" * V"msg-id"^1 * V"CRLF", -- RFC 2822 Section 3.6.4
	["references"] = P"References:" * V"msg-id"^1 * V"CRLF", -- RFC 2822 Section 3.6.4
	["msg-id"] = V"CFWS"^-1 * P"<" * V"id-left" * P"@" * V"id-right" * P">" * V"CFWS"^-1, -- RFC 2822 Section 3.6.4
	["id-left"] = V"dot-atom-text" + V"no-fold-quote" + V"obs-id-left", -- RFC 2822 Section 3.6.4
	["id-right"] = V"dot-atom-text" + V"no-fold-literal" + V"obs-id-right", -- RFC 2822 Section 3.6.4
	["no-fold-quote"] = P"\"" * (V"qtext" + V"quoted-pair")^0 * P"\"", -- RFC 2822 Section 3.6.4
	["no-fold-literal"] = P"[" * (V"dtext" + V"quoted-pair")^0 * P"]", -- RFC 2822 Section 3.6.4
}
end

local obs_strict = {
	["obs-FWS"] = lpeg.P(false),
	["obs-qp"] = lpeg.P(false),
	["obs-phrase"] = lpeg.P(false),
	["obs-utext"] = lpeg.P(false),
	["obs-day-of-week"] = lpeg.P(false),
	["obs-year"] = lpeg.P(false),
	["obs-month"] = lpeg.P(false),
	["obs-day"] = lpeg.P(false),
	["obs-hour"] = lpeg.P(false),
	["obs-minute"] = lpeg.P(false),
	["obs-second"] = lpeg.P(false),
	["obs-zone"] = lpeg.P(false),
	["obs-angle-addr"] = lpeg.P(false),
	["obs-mailbox-list"] = lpeg.P(false),
	["obs-address-list"] = lpeg.P(false),
	["obs-local-part"] = lpeg.P(false),
	["obs-domain"] = lpeg.P(false),
	["obs-fields"] = lpeg.P(false),
	["obs-id-left"] = lpeg.P(false),
	["obs-id-right"] = lpeg.P(false),
}

local os = setmetatable({}, {__index = function() return lpeg.P(false) end})

local join_set = function (...)
	local n = select('#', ...)
	local ret = {}
	for i = 1, n do
		local t = select(i, ...)
		if type(t)=='table' then
			for k, v in pairs(t) do
				ret[k] = v
			end
		elseif type(t)=='string' then
			ret[1] = t
		elseif type(t)=='boolean' then
			-- TODO: check no overwrite
		else
			error('join_set: bad argument number '..i..' of type '..type(t))
		end
	end
	return ret
end

local gr = join_set(core_values, lex_tokens(), date_time(), address(), overall_message(), fields(), obs_strict)

return gr

