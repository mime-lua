#!/usr/bin/lua
--
-- Copyright (c) 2009 Mauro Iazzi
--
-- Permission is hereby granted, free of charge, to any person
-- obtaining a copy of this software and associated documentation
-- files (the "Software"), to deal in the Software without
-- restriction, including without limitation the rights to use,
-- copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
-- OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
-- HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
-- WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
-- OTHER DEALINGS IN THE SOFTWARE.
--/


--require 'my'
require 'lpeg'

local gr = dofile'mime.lua'

local match = function (s, r, c)
	c= c or s
	local ret = { string = s, rule = r, match = true, capture = c }
	return ret
end

local tests = {
	{ string = '\r\n ', rule = 'FWS', match = true, capture = ' ' },
	{ string = '"rrr\r\n aaa"', rule = lpeg.Cs(lpeg.V'quoted-string'), match = true, capture = '"rrr aaa"'},
	{ string = '"rrr  \\$\r\n aaa"', rule = lpeg.Cs(lpeg.V'quoted-string'), match = true, capture = '"rrr $ aaa"'},
	{ string = '(some stupid @(comment) \\( ** \r\n )', rule ='comment', match = true, capture = ' '},
	{ string = 'single_atom_1', rule ='atom', match = true, capture = 'single_atom_1'},
	{ string = '2nd{atom}2', rule ='atom', match = true, capture = '2nd{atom}2'},
	{ string = '2nd{atom}2.&checking!', rule ='dot-atom', match = true, capture = '2nd{atom}2.&checking!'},
	{ string = '\r\n (another comment ) \t\r\n (id %(crmnr) \\( ** \r\n )   ', rule ='CFWS', match = true, capture = ' '},
	{ string = '()', rule ='CFWS', match = true, capture = ' '},
	{ string = 'Icanwrite"a \r\n simple"phrase', rule ='phrase', match = true, capture='Icanwrite"a simple"phrase'},
	{ string = 'Thu, 2\r\n Apr 2009 14:36:04 +0000', rule ='date-time', match = true, capture={ 'Thu, 2 Apr 2009 14:36:04 +0000', weekday='Thu', year='2009', month='Apr', day='2', zone='+0000', minute='36', hour='14', second='04' }},
	{ string = '(a CFWS) \r\n (that ends here)[ 127.0.0.1:8888 \r\n oooo]', rule ='domain-literal', match = true, capture=' [ 127.0.0.1:8888 oooo]'},
	{ string = 'mauro.iazzi@gmail.com', rule ='addr-spec', match = true, capture={ [1]='mauro.iazzi@gmail.com', box='mauro.iazzi', domain='gmail.com' }, },
	{ string = 'mauro.iazzi@gmail.com', rule ='address', match = true, capture={ [1]='mauro.iazzi@gmail.com', box='mauro.iazzi', domain='gmail.com' }, },
	{ string = '"Mauro Iazzi" <"mauro\\2\r\n .iazzi"@gmail.com>', rule ='address', match = true, capture={ [1]='"Mauro Iazzi" <"mauro2.iazzi"@gmail.com>', box='mauro.iazzi', domain='gmail.com' }, },
	{ string = '"Mauro \r\n Iazzi" (a comment?) <mauro.iazzi@gmail.com>', rule ='address', match = true, capture={ [1]='"Mauro Iazzi" <mauro.iazzi@gmail.com>', box='mauro.iazzi', domain='gmail.com' }, },
	{ string = '<mauro.iazzi@gmail.com>', rule ='msg-id', match = true, },
	{ string = '<"sent\\ by\\ \\"M.I.\\"mauro.iazzi"@[from\\ domain\\ gmail.com]>', rule ='msg-id', match = true, },
}

local dee = function (...)
	print(...)
	return ...
end

local gather = function (...)
	local n = select('#', ...)
	return n==1 and ... or { ... }
end

local function equal (a, b)
	dee('', a, b)
	if a==b then return true end
	if type(a)~=type(b) then return false end
	if type(a)=='table' then
		for k, v in pairs(a) do
			if not equal(v, b[k]) then return false end
		end
		for k, v in pairs(b) do
			if not equal(v, a[k]) then return false end
		end
		return true
	end
	return false
end

local do_test = function (i, t)
	local s, r, m, c = t.string, t.rule, t.match, t.capture
	local rn
	if type(r)=='string' then
		r = lpeg.V(r)
	end
	gr[1] = r / gather * lpeg.Cp()
	local patt = lpeg.P(gr)
	local ret, n = dee(patt:match(s))
	if m then
		assert(ret, 'test '..i..' failed by not matching a conforming string')
		assert(n==#s+1, 'test '..i..' failed by not matching the full string (match was '..string.sub(s, 1, n-1)..')')
		if c then
			assert(equal(ret, c), 'test '..i..' failed by returning the wrong capture '..tostring(ret)..' instead of '..tostring(c))
		end
	else
		assert(ret==nil, 'test '..i..' failed by matching a wrong string')
	end
end
print("starting the tests ...")
for i, t in ipairs(tests) do
	io.write('performing test ' .. i .. ' on rule ' .. tostring(t.rule))
	io.flush()
	local st, err = pcall(do_test, i, t)
	if st then err = 'success' end
	print('', err)
end


